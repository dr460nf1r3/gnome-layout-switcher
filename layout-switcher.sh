#!/bin/bash 
function goto
{
    label=$1
    cmd=$(sed -n "/^:[[:blank:]][[:blank:]]*${label}/{:a;n;p;ba};" $0 | 
          grep -v ':$')
    eval "$cmd"
    exit
}

function garuda 
{
    gnome-extensions disable material-shell@papyelgringo
    gnome-extensions disable dash-to-panel@jderose9.github.com
    gnome-extensions disable pop-shell@system76.com
    gnome-extensions enable dash-to-dock@micxgx.gmail.com
    gnome-extensions enable arc-menu@linxgem33.com
    gnome-extensions enable unite@hardpixel.eu
    gnome-extensions enable user-theme@gnome-shell-extensions.gcampax.github.com
    gnome-extensions enable transparent-window-moving@noobsai.github.com
    gnome-extensions enable transparent-panel@fthx
    gnome-extensions enable appindicatorsupport@rgcjonas.gmail.com
    gnome-extensions enable arch-update@RaphaelRochet
    gnome-extensions enable blyr@yozoon.dev.gmail.com
    gnome-extensions enable gamemode@christian.kellner.me
    gnome-extensions enable goodbyegdmflick@pratap.fastmail.fm
    gnome-extensions enable gsconnect@andyholmes.github.io
    gnome-extensions enable noannoyance@daase.net
    gnome-extensions enable remove-alt-tab-delay@daase.net
    gnome-extensions enable sound-output-device-chooser@kgshank.net
    gnome-extensions enable trayIconsReloaded@selfmade.pl
    gnome-extensions enable tweaks-system-menu@extensions.gnome-shell.fifi.org
    notify-send "Garudas layout applied 🥰"
}

function gnome
{
    gnome-extensions disable dash-to-panel@jderose9.github.com
    gnome-extensions disable dash-to-dock@micxgx.gmail.com
    gnome-extensions disable arc-menu@linxgem33.com
    gnome-extensions disable unite@hardpixel.eu
    gnome-extensions disable pop-shell@system76.com
    gnome-extensions disable material-shell@papyelgringo
    gnome-extensions disable user-theme@gnome-shell-extensions.gcampax.github.com
    gnome-extensions enable transparent-window-moving@noobsai.github.com
    gnome-extensions enable transparent-panel@fthx
    gnome-extensions enable appindicatorsupport@rgcjonas.gmail.com
    gnome-extensions enable arch-update@RaphaelRochet
    gnome-extensions enable blyr@yozoon.dev.gmail.com
    gnome-extensions enable gamemode@christian.kellner.me
    gnome-extensions enable goodbyegdmflick@pratap.fastmail.fm
    gnome-extensions enable gsconnect@andyholmes.github.io
    gnome-extensions enable noannoyance@daase.net
    gnome-extensions enable remove-alt-tab-delay@daase.net
    gnome-extensions enable sound-output-device-chooser@kgshank.net
    gnome-extensions enable trayIconsReloaded@selfmade.pl
    gnome-extensions enable tweaks-system-menu@extensions.gnome-shell.fifi.org
    notify-send "GNOME layout applied 🙌🏻"
}

function windows
{
    gnome-extensions disable transparent-panel@fthx
    gnome-extensions disable unite@hardpixel.eu
    gnome-extensions disable pop-shell@system76.com
    gnome-extensions disable dash-to-dock@micxgx.gmail.com
    gnome-extensions disable user-theme@gnome-shell-extensions.gcampax.github.com
    gnome-extensions disable material-shell@papyelgringo
    gnome-extensions enable dash-to-panel@jderose9.github.com
    gnome-extensions enable arc-menu@linxgem33.com
    gnome-extensions enable appindicatorsupport@rgcjonas.gmail.com
    gnome-extensions enable arch-update@RaphaelRochet
    gnome-extensions enable blyr@yozoon.dev.gmail.com
    gnome-extensions enable gamemode@christian.kellner.me
    gnome-extensions enable goodbyegdmflick@pratap.fastmail.fm
    gnome-extensions enable gsconnect@andyholmes.github.io
    gnome-extensions enable noannoyance@daase.net
    gnome-extensions enable remove-alt-tab-delay@daase.net
    gnome-extensions enable sound-output-device-chooser@kgshank.net
    gnome-extensions enable trayIconsReloaded@selfmade.pl
    gnome-extensions enable tweaks-system-menu@extensions.gnome-shell.fifi.org
    notify-send "Windows layout applied 💻"

}

function tiling
{
    gnome-extensions disable dash-to-panel@jderose9.github.com
    gnome-extensions disable dash-to-dock@micxgx.gmail.com
    gnome-extensions disable unite@hardpixel.eu
    gnome-extensions disable arc-menu@linxgem33.com
    gnome-extensions disable material-shell@papyelgringo
    gnome-extensions enable user-theme@gnome-shell-extensions.gcampax.github.com
    gnome-extensions enable transparent-window-moving@noobsai.github.com
    gnome-extensions enable transparent-panel@fthx
    gnome-extensions enable pop-shell@system76.com
    gnome-extensions enable appindicatorsupport@rgcjonas.gmail.com
    gnome-extensions enable arch-update@RaphaelRochet
    gnome-extensions enable blyr@yozoon.dev.gmail.com
    gnome-extensions enable gamemode@christian.kellner.me
    gnome-extensions enable goodbyegdmflick@pratap.fastmail.fm
    gnome-extensions enable gsconnect@andyholmes.github.io
    gnome-extensions enable noannoyance@daase.net
    gnome-extensions enable remove-alt-tab-delay@daase.net
    gnome-extensions enable sound-output-device-chooser@kgshank.net
    gnome-extensions enable trayIconsReloaded@selfmade.pl
    gnome-extensions enable tweaks-system-menu@extensions.gnome-shell.fifi.org
    notify-send "You can enable tiling using the tray icon now 🤔"
}

echo "Choose a layout to apply:"
echo "1) Garuda"
echo "2) GNOME"
echo "3) Windows"
echo "4) PopOS-Tiling"

: start
read layout_num

if [ $layout_num = "1" ]; then 
    garuda
    echo "You chose Garuda as layout, applying.."
elif [ $layout_num = "2" ]; then
    gnome
    echo "You chose GNOME as layout, applying.."
elif [ $layout_num = "3" ]; then 
    windows
    echo "You chose Windows as layout, applying.."
elif [ $layout_num = "4" ]; then 
    tiling
    echo "You chose PopOS-Tiling as layout, applying.."
else echo "Thats not a valid choice!"
    goto "start"
fi